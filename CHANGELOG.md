## [1.0.8](https://gitlab.com/jonathang/tbc-semantic-release-test-project/compare/1.0.7...1.0.8) (2022-10-15)


### Bug Fixes

* test with a bad GPG key ([001aa34](https://gitlab.com/jonathang/tbc-semantic-release-test-project/commit/001aa34b7cf85a3fd527ac0cba76f1649233bbd0))
* testing with a valid gpg key ([27dcbf1](https://gitlab.com/jonathang/tbc-semantic-release-test-project/commit/27dcbf1a28af914d3ab6def494abaf2b057ba7be))
* testing with a valid key ([95332e4](https://gitlab.com/jonathang/tbc-semantic-release-test-project/commit/95332e4df36f70949b3a2a585ebd648576510702))
* testing with valid gpg key ([35724c7](https://gitlab.com/jonathang/tbc-semantic-release-test-project/commit/35724c7f08796a410cd368563d746dc63bacced0))

## [1.0.7](https://gitlab.com/jonathang/tbc-semantic-release-test-project/compare/1.0.6...1.0.7) (2022-10-15)


### Bug Fixes

* test without GPG key ([4d553f2](https://gitlab.com/jonathang/tbc-semantic-release-test-project/commit/4d553f297a7ed7dee1ebdbb6dc179f8ca8b4cc8f))

## [1.0.6](https://gitlab.com/jonathang/tbc-semantic-release-test-project/compare/1.0.5...1.0.6) (2022-10-15)


### Bug Fixes

* test after MR review   changes ([70bf844](https://gitlab.com/jonathang/tbc-semantic-release-test-project/commit/70bf844ade8828b45895c056d9568fabc485f306))

## [1.0.5](https://gitlab.com/jonathang/tbc-semantic-release-test-project/compare/1.0.4...1.0.5) (2022-10-14)


### Bug Fixes

* final test before MR ([33e880d](https://gitlab.com/jonathang/tbc-semantic-release-test-project/commit/33e880d155f630d4e09d0aab5a55be4991d6f48d))

## [1.0.4](https://gitlab.com/jonathang/tbc-semantic-release-test-project/compare/1.0.3...1.0.4) (2022-10-13)


### Bug Fixes

* test ([52c0063](https://gitlab.com/jonathang/tbc-semantic-release-test-project/commit/52c0063569edf101ab4e1373de44b854c6e0dfa2))

## [1.0.3](https://gitlab.com/jonathang/tbc-semantic-release-test-project/compare/1.0.2...1.0.3) (2022-10-13)


### Bug Fixes

* test ([e8f73b4](https://gitlab.com/jonathang/tbc-semantic-release-test-project/commit/e8f73b4242c2561afcf2084e5c88250d3ced1c1c))

## [1.0.2](https://gitlab.com/jonathang/tbc-semantic-release-test-project/compare/1.0.1...1.0.2) (2022-10-13)


### Bug Fixes

* enable changelogs to make semrel commit ([9f0efac](https://gitlab.com/jonathang/tbc-semantic-release-test-project/commit/9f0efac934cc0fa62f0e639d3ea92286ea3e62db))
